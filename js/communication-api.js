// lit la réponse du serveur
// response:http response 
// retourne le json envoyé par l'API
const readResponse = (response) => {
    //si la réponse est ok (code 200)
    if (response.ok) {
        //retoune le corps de la réponse sous forme de json
        return response.json();
    } else {
        //autrement averti l'utilisateur en indiquant les codes de retour
        alert(`ouch problème de serveur ! ${response.status} : ${response.statusText}`);
    }
}

// gére quand il y a une erreur
// error: js error
const handleError = (error) => {
    console.log(error);
}

// point d'entrée de la connexion avec l'api
// permet de contruire le rendu de la page
//url [string] : url du endpoint(api) à contacter 
//container [élément hmlt] : élément html contenant la liste des cameras
const fetchCameraApi = (url, container) => {
    fetch(url)
        //ensuite, si la réponse est ok , me renvoie le json de cette réponse
        .then(response => readResponse(response))
        //ensuite, vu que tu as le json, construit moi le DOM à partir des données
        .then(cameras => mapApiResults(cameras, container))
        //mais si une erreur arrive , affiche la moi dans la console
        .catch(error => handleError(error));
}

//envoi la commande coté server
//url [string] : url du endpoint(api) à contacter 
//order [object] : commande a passer
const fetchOrderApi = (url, order) => {
    //initialise les options de la requete
    const options = {
        method: "POST",
        headers: { 'content-type': 'application/json' },
        mode: 'cors'
    };
    // ajoute la commande sérialisé au corps de la requete
    options.body = JSON.stringify(order);
    //envoi la commande a l'API avec les options
    fetch(url, options)
        //ensuite lis la réponse du server
        .then(response => readResponse(response))
        //ensuite stocke la commande validé en local (avec l'id de commande)
        .then(ordered => addOrder(ordered))

    //mais si une erreur arrive , affiche la moi dans la console
    .catch(error => handleError(error));

}