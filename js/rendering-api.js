// id du select de ma lense
const focusSelect = "focusSelect";

// id de l'input des quantités
const inputQte = "inputQte";

// id du bouton d'ajout au panier
const addBtnName = "btnAddToCart";

//bouton validation de commande
const btnValidOrder = "btnValidOrder";

//popup de validation d'ajout au panier
const popup = document.querySelector(".popup");

// btn close de la popup
const close = document.querySelector(".closePopup");


// créé le html interne de mon select
// lenses: tableau de chaine de cararctére
// retourne une chaine de caractére contenant les items entourés du tag html option
const createLensesDisplay = (lenses) => {
    return lenses.map(l => (
        `<option value="${l}">${l}</option>`
    )).join('');
}

// abonné à lévènement de selection des lentilles 
//ajoute la lentille choisi en data attribut au bouton de validation 
const addLenseToButton = (e) => {
    // Je récupére le bouton que j'ai créé dans ma page détail
    let addToCart = document.getElementById(addBtnName);
    //je récupère la valeur selectionné
    let val = e.target.value;
    //je l'ajoute au dataset du bouton
    addToCart.dataset.lense = val;
}

// abonné à l' évènement de changement de quantité
//ajoute la quantité saisi en data attribut au bouton de validation
const addQteToButton = (e) => {
    // Je récupére le bouton que j'ai créé dans ma page détail
    let addToCart = document.getElementById(addBtnName);
    //je récupère la valeur selectionné
    let val = e.target.value;
    //je l'ajoute au dataset du bouton
    addToCart.dataset.qte = val;
}

//création du tableau de ma page panier
//basket [tableau d'object]: tableau d'éléments mis dans le stockage
//container [élément html]: élément html parent du tableau a afficher
const createBasketHtml = (basket, container) => {

    //crée l'entete du tableau
    //puis crée le slignes du tableau
    //puis crée le footer du tableau
    container.innerHTML = (`
        <table class="BasketTable">
            <tr class="cellTitle">
                <td class="titleTable tableName">Désignation</td>
                <td class="titleTable tableLense">Lentilles</td>
                <td class="titleTable tableAmount">Quantité</td>
                <td class="titleTable tableUnitPrice">Prix unitaire</td>
                <td class="titleTable tableTotal">Total</td>
            </tr>  
    
    ${basket.map((bi) => creatBasketItemHtml(bi)).join('')}
     
    <tr>
        <td class="tableName"></td>
        <td class="tableLense"></td>
        <td class="tableAmount"></td>
        <td class="tableUnitPrice"></td>
        <td class="tableTotal">Prix total : ${basket.map((bi)=>bi.qte * bi.price).reduce((a, b) => a+b,0)}</td>
    </tr>
    
        </table>
    `);
    //récupère le bouton de validation de la commande
    let btnSubmit = document.querySelector("#btnValidOrder");
    // Si le bouton existe
    if (btnSubmit != null) {
        //abonne la création de la commande au clic du bouton
        btnSubmit.addEventListener("click", (e) => {
            //arrête la propagation de l'évènement
            e.preventDefault();
            //fonction de création de la commande
            createOrder();
            //arrête la propagation de l'évènement
            return false;
        });
    }
}

//crée le html correspondant à un item du panier
//html sous forme de tableau (tr)
//bi [Object] : item contenu dans le panier
const creatBasketItemHtml = (bi) => {
    return `
            <tr class="cellTitle">
                <td class="tableName">
                    ${bi.name}
                    </br>
                    <img class="basketItemImg" src="${bi.imgUrl}" />
                </td>
                <td class="tableLense">${bi.lense}</td>
                <td class="tableAmount">${bi.qte}</td>
                <td class="tableUnitPrice">${bi.price}€</td>
                <td class="tableTotal">${bi.price * bi.qte}€</td>
            </tr> 
    
        `;
}

// créé le html d'un item
// jsonObject [Object]: dans notre cas: les informations liées à une caméra
//             signature en début de page
// asListItem [Boolean]: construit un html dont le tag exterieur est un li
//             ou dont le tag exterieur est une div
const createCameraHtmlCard = (jsonObject, asListItem) => {
    //si l'item est contenu dans une liste 
    if (asListItem) {
        //renvoi l'objet comme un item de liste
        return `
                    <li id="${jsonObject._id}" class="cameraItem">
                        <a href="detail.html?id=${jsonObject._id}">
                            <input type="hidden" value="${jsonObject._id}"/>
                            <img class="cameraImg" src="${jsonObject.imageUrl}" />
                            <h3 class="cameraName">${jsonObject.name}</h3>
                            <label class="cameraPrice">${jsonObject.price}€</label>
                            <label class="moreInformation"><i class="fas fa-search-plus"></i></label>
                        </a>
                    </li>
                    `;
    } else {
        //autrement renvoi l'objet sous forme de div
        return `
                    <div id="${jsonObject._id}" class="cameraDetailItem">
                        
                            <input type="hidden" value="${jsonObject._id}"/>
                            <div class="cameraDetailImg" >
                                <img src="${jsonObject.imageUrl}" />
                            </div>    
                            <div class="cameraDetailText">
                                <h3 class="cameraDetailName">${jsonObject.name}</h3>
                                <p class="cameraDescriptif">${jsonObject.description}</p>
                                <select id="${focusSelect}">
                                    <option value="select">Sélectionnez votre lentille</option>
                                    ${createLensesDisplay(jsonObject.lenses)}
                                </select>
                                <label class="cameraDetailPrice">${jsonObject.price}€</label>
                                <label id="qte">Quantité: </label>
                                <input id="${inputQte}" type="number"  min="1" max="10" step="1" placeholder="Séléctionnez une quantité"/>
                                
                                <button id="${addBtnName}" data-id="${jsonObject._id}" data-img="${jsonObject.imageUrl}" " data-name="${jsonObject.name}" data-price="${jsonObject.price}" ><i class="fas fa-shopping-bag"></i> Ajoutez au panier</button>
                            </div>    
                        
                    </div>
                    `;
    }

}

// creation du html de la page commande 
// store [tableau d'objets] : liste des commandes stockés en local
//container [élément html] : container de la liste de commande
const createOrderHtml = (store, container) => {
    //RG : seul la derniere commande dois être affiché
    //on récupère le premier item du tableau
    let o = store[0];
    // initialise le stockage local du total
    let total = 0.0;
    //calcule le prix total de la commande
    o.products.forEach(x => {
            total = total + x.price;
        })
        //insère le html construit dans le container
    container.innerHTML = (`
    <div class="orderHistory">
        <h2 class="orderTitle">Merci pour votre commande !</h2>
        <p class="orderId">Numéro de votre commande : <strong>${o.orderId}</strong></p>
        <p class="orderPrice">Pour un montant de : <strong>${total}€</strong></p>
    </div>
    `);
}

// construit le html pour les données de l'API get
// apiResult [Object]: retour de l'API jsonifié
// container [HTML]: element contenant le rendu
const mapApiResults = (apiResult, container) => {
    // vérifie si le resultat de l'api est un tableau ou un seul objet
    let isArr = Array.isArray(apiResult);
    // si c'est un tableau
    if (isArr) {
        // on ajoute au container
        container.innerHTML = (
            // le html de tous les elements du tableau
            apiResult.map(c => createCameraHtmlCard(c, true)).join('')
        );
    } else {
        // sinon on ajoute au container
        container.innerHTML = (
            // le html du détail d'un item
            createCameraHtmlCard(apiResult, false)
        );

        /// pour les besoins du panier
        //je récupere le choix de la lense
        let choiceLense = document.getElementById(focusSelect);
        //j'ajoute un écouteur sur le changement de lentille à la fonction d'ajout cf: commentaire sur addLenseToButton
        choiceLense.addEventListener("change", (e) => addLenseToButton(e));

        //je récupere le choix de la quantité
        let choiceQte = document.getElementById(inputQte);
        //j'ajoute un écouteur sur le changement de la quantité à la fonction d'ajout cf: commentaire sur addQteToButton
        choiceQte.addEventListener("change", (e) => addQteToButton(e));

        // Je récupére le bouton que j'ai créé dans ma page détail
        let addToCart = document.getElementById(addBtnName);
        // j'ajoute un écouteur qui se déclenche lors du click du bouton
        addToCart.addEventListener("click", (e) => addItemToBasket(e));
    }
}

// Créé le rendu de la page basket
//container [élément html] : élément parent contenant le panier
const mapBasketItems = (container) => {
    //récupèration du panier stocké
    let store = getBasketStore();
    //création du html pour le panier
    createBasketHtml(store, container);
}

// construit le html de la page commande 
//container [élément html] : élément parent contenant la commande
const mapOrderResults = (container) => {
    //récupèration de la commande stocké
    let store = getOrderStore();
    //création du html pour la commande
    createOrderHtml(store, container);
}