// declare le nom du panier (basket) dans le local storage
const basketStoreName = "basket";

//déclare le nom du formulaire (user) dans le local storage
const formStoreName = "user";

//déclare le nom de la commande (order) dans le local storage
const orderStoreName = "order";

// stockage de l'url courante
let currentPageURL = "";

////////////////création de mes stokages ////////////////////////////////// 
// creation de mon stokage panier, 
// à appeller au chargement de la page panier
const createBasketStore = () => {
    //récupère si il existe le local storag epour le panier
    let store = JSON.parse(window.localStorage.getItem(basketStoreName));
    //si il n'est pas initialisé
    if (store == null) {
        //initialise le stockage local pour le panier
        initBasketStore();
    }
}

//réinitialise le tableau
const initBasketStore = () => {
    // j'initialise mon tableau
    window.localStorage.setItem(basketStoreName, JSON.stringify([]));
}

// création de mon stokage formulaire
const createUserStore = () => {
    //récupère si il existe le local storag epour le formulaire
    let store = JSON.parse(window.localStorage.getItem(formStoreName));
    //si il n'est pas initialisé
    if (store == null) {
        //initialise le stockage local pour le formulaire
        window.localStorage.setItem(formStoreName, JSON.stringify(new formItem("", "", "", "", "", "", "")));
    }
}

//creation de mon stockage commande 
const createOrderStore = () => {
    //récupère si il existe le local storag epour la commande
    let store = JSON.parse(window.localStorage.getItem(orderStoreName));
    //si il n'est pas initialisé
    if (store == null) {
        //initialise le stockage local pour la commande
        window.localStorage.setItem(orderStoreName, JSON.stringify([]));
    }
}

// récuperation de mon formulaire dans le local storage
const getUserStore = () => {
    return JSON.parse(window.localStorage.getItem(formStoreName));
}

// récupération de mon panier dans le local storage
const getBasketStore = () => {
    return JSON.parse(window.localStorage.getItem(basketStoreName));
}

// récupération de ma commande dans le local storage 
const getOrderStore = () => {
    return JSON.parse(window.localStorage.getItem(orderStoreName));
}

//mise a jour de mon formulaire dans le local storage
//contact [objet] : objet contenant les données du formulaire
const updateUserStore = (contact) => {
    return window.localStorage.setItem(formStoreName, JSON.stringify(contact));
}

//mise a jour de l'objet contact 
//lastname[string ] : valeur du formulaire pour le lastname
const updateContactLastName = (lastName) => {
    //récupère l'objet de stockage du formulaire
    let contact = getUserStore();
    //indique si la valeur est valide
    contact.validLastName = lastName != null;
    //stocke la valeur dans l'objet
    contact.lastName = lastName;
    //enregistre l'objet en local
    updateUserStore(contact);
}

//firstName[string ] : valeur du formulaire pour le firstName
const updateContactFirstName = (firstName) => {
    //récupère l'objet de stockage du formulaire
    let contact = getUserStore();
    //indique si la valeur est valide
    contact.validFirstName = firstName != null;
    //stocke la valeur dans l'objet
    contact.firstName = firstName;
    //enregistre l'objet en local
    updateUserStore(contact);
}

//address[string ] : valeur du formulaire pour le address
const updateContactAddress = (address) => {
    //récupère l'objet de stockage du formulaire
    let contact = getUserStore();
    //indique si la valeur est valide
    contact.validAddress = address != null;
    //stocke la valeur dans l'objet
    contact.address = address;
    //enregistre l'objet en local
    updateUserStore(contact);
}

//city[string ] : valeur du formulaire pour le city
const updateContactCity = (city) => {
    //récupère l'objet de stockage du formulaire
    let contact = getUserStore();
    //indique si la valeur est valide
    contact.city = city != null;
    //stocke la valeur dans l'objet
    contact.city = city;
    //enregistre l'objet en local
    updateUserStore(contact);
}

//email[string ] : valeur du formulaire pour le email
const updateContactEmail = (email) => {
    //récupère l'objet de stockage du formulaire
    let contact = getUserStore();
    //indique si la valeur est valide
    contact.email = email != null;
    //stocke la valeur dans l'objet
    contact.email = email;
    //enregistre l'objet en local
    updateUserStore(contact);
}

//mise à jour de mon panier dans le local storage
//basket [tableau d objet] : contient les items de mon panier
const updateBasketStore = (basket) => {
    return window.localStorage.setItem(basketStoreName, JSON.stringify(basket));
}

//mise à jour de ma commande dans le local storage 
//orders [tableau d objet] : contient l'historique des commandes
const updateOrderStore = (orders) => {
    return window.localStorage.setItem(orderStoreName, JSON.stringify(orders));
}

// ajoute un item au panier
// e [Event]: evenement onclick du bouton de validation
const addItemToBasket = (e) => {
    //récupere le stockage du panier
    let basket = getBasketStore();
    //ajoute les informations stocké dans le dataset du bouton
    let i = e.target.dataset.id;
    let img = e.target.dataset.img;
    let n = e.target.dataset.name;
    let p = e.target.dataset.price;
    let l = e.target.dataset.lense;
    let q = e.target.dataset.qte;

    //choix de la lentille et de la quantité
    if (l === undefined || l === "select") {
        alert('Veuillez choisir votre lentille');
    } else if (q == undefined || parseInt(q) <= 0) {
        alert('Veuillez choisir une quantité');

    } else {
        //crée un nouvel item de panier
        let item = new basketItem(i, n, img, p, q, l);
        //le place dans le panier
        basket.push(item);
        //enregistre les modifications
        updateBasketStore(basket);

        //popup ajout au panier ! 
        popup.classList.toggle("show");
        close.onclick = () => {
            popup.classList.toggle("show");
        }
    }
}

//création d'une commande
const createOrder = () => {
    //initialise le tableau des id a envoyer
    let ids = [];
    //récupère le panier stocké en local
    let items = getBasketStore();
    // faire une boucle foreach pour ajouter chaque id dans le tableau//
    //pour tout les items dans mon panier je rajoute l id de l item dans mon tableau ids
    items.forEach((b) => {
        ids.push(b.id);
    });

    // je recupere mon objet contact 
    let contact = getUserStore();

    //Je verifie que mes champs sont remplis
    let isValidLn = contact.validLastName || contact.lastName != null;
    if (!isValidLn) {
        errorDisplay("lastName", "Ce champs est obligatoire");
    }
    let isValidFn = contact.validFirstName || contact.FirstName != null;
    if (!isValidFn) {
        errorDisplay("firstName", "Ce champs est obligatoire");
    }
    let isValidAd = contact.validAddress || contact.address != null;
    if (!isValidAd) {
        errorDisplay("address", "Ce champs est obligatoire");
    }
    let isValidCi = contact.validCity || contact.city != null;
    if (!isValidCi) {
        errorDisplay("city", "Ce champs est obligatoire");
    }
    let isValidEm = contact.validEmail || contact.email != null;
    if (!isValidEm) {
        errorDisplay("email", "Ce champs est obligatoire");
    }

    //Je vérifie que mon panier contient des items
    let isValidBasket = ids.length > 0;
    if (!isValidBasket) {
        alert("Votre panier est vide !");
    }
    //une fois que tout est valide je créer ma commande et je l'envoie
    if (isValidAd && isValidBasket && isValidLn && isValidFn && isValidCi && isValidEm) {
        let o = new order(ids, contact);
        fetchOrderApi("http://localhost:3000/api/cameras/order", o);
    }

}

//pour stoker le resultat de ma commande (identifiant , commande ...)
//order [objet] : contient la commande validé par le server
const addOrder = (order) => {
    //récupèr el'historique des commande
    let orders = getOrderStore();

    //ajouet la commande nouvellement passé
    orders.push(order);
    //mets à jour l'historique
    updateOrderStore(orders);
    //réinitialise le panier
    initBasketStore();
    //redirige vers la page commande
    window.location.href = "commande.html";
}

// initialisation des store dans le local storage
const initializeStore = () => {
    createBasketStore();
    createUserStore();
    createOrderStore();
}