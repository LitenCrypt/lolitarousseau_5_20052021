const urlDetail = 'http://localhost:3000/api/cameras/';

// Récupération de l'id dans l'url de la page pour pouvoir
// passer dans l'url lors du chargement via l'API
// url [string] : url a appeler pour le detail d'une camera
const getDetailUrl = (url) => {
    //récupère les parametres pasés via l'url
    let detailObject = new URLSearchParams(window.location.search);
    //construit l'url avec la base + l'id récupérer dns l'url de la page
    let u = url + detailObject.get('id');
    // retourn l'url formaté
    return u;
}

// à la fin du chargement de toutes mes ressources
window.addEventListener("load", function(e) {
    // appel la récupération de données
    fetchCameraApi(getDetailUrl(urlDetail), document.getElementById('containerDetail'));
    // Initialize le store au cas ou un panier es necessaire
    initializeStore();
});