const inputs = document.querySelectorAll('input[type="text"], input[type="mail"]');

//validation de mon formulaire 
//tag [string] : element distinctif de la class du container à récuperer
//message [string] : message a afficher
//valide[boolean] : indique si la valeur est valide ou pas
const errorDisplay = (tag, message, valid) => {
    const container = document.querySelector("." + tag + "-container");
    const span = document.querySelector("." + tag + "-container > span");

    //si ce n'est pas valide 
    if (!valid) {
        //ajoute la classe error au container
        container.classList.add("error");
        span.classList.add("error");
        span.textContent = message;
    } else {
        //autrement surpprime la classe erreur du container
        container.classList.remove("error");
        span.textContent = message;
    }
};

//vérifie la valeur saisi de l'input
//value [string] : stocke la valeur saisi
const lastNameChecker = (value) => {
    //si la valeur n'est pas comprise entre 3 et 30 caracteres 
    if (value.length > 0 && (value.length < 3 || value.length > 30)) {
        //renvoi un message d'erreur
        errorDisplay("lastName", "Le nom doit faire entre 3 et 30 caractères");
        //remet à zero la valeur stocké
        updateContactLastName(null);
        //autrement si la valeur ne correspond pas au regle de la regex
    } else if (!value.match(/^[a-zA-Zéèàêû_.-\s]*$/)) {
        //envoie un message d'erreur
        errorDisplay("lastName", "Le nom ne doit pas contenir de caractères spéciaux");
        //remet à zero la valeur stocké
        updateContactLastName(null);
        //autrement si la valeur est null
    } else if (value.length == 0) {
        //envoie un message d'erreur
        errorDisplay("lastName", "Ce champs est obligatoire");
        //remet à zero la valeur stocké
        updateContactLastName(null);
        //si la valeur saisi est correct 
    } else {
        //enlève le message d'erreur
        errorDisplay("lastName", "", true)
            //stock de ma valeur
        updateContactLastName(value);
    }
};

//vérifie la valeur saisi de l'input
//value [string] : stocke la valeur saisi
const firstNameChecker = (value) => {
    //si la valeur n'est pas comprise entre 2 et 30 caracteres
    if (value.length > 0 && (value.length < 2 || value.length > 30)) {
        //renvoi un message d'erreur
        errorDisplay("firstName", "Le prénom doit faire entre 3 et 30 caractères");
        //remet à zero la valeur stocké
        updateContactFirstName(null);
        //autrement si la valeur ne correspond pas au regle de la regex
    } else if (!value.match(/^[a-zA-Zéèàêû_.-\s]*$/)) {
        //renvoi un message d'erreur
        errorDisplay("firstName", "Le prénom ne doit pas contenir de caractères spéciaux");
        //remet à zero la valeur stocké
        updateContactFirstName(null);
        //autrement si la valeur est null
    } else if (value.length == 0) {
        //renvoi un message d'erreur
        errorDisplay("firstName", "Ce champs est obligatoire");
        //remet à zero la valeur stocké
        updateContactFirstName(null);
        //si la valeur saisi est correct
    } else {
        //enlève le message d'erreur
        errorDisplay("firstName", "", true)
            //stock de ma valeur
        updateContactFirstName(value);
    }
};

//vérifie la valeur saisi de l'input
//value [string] : stocke la valeur saisi
const addressChecker = (value) => {
    //si la valeur n'est pas comprise entre 3 et 30 caracteres
    if (value.length > 0 && (value.length < 2 || value.length > 50)) {
        //renvoi un message d'erreur
        errorDisplay("address", "L'adresse doit faire entre 2 et 50 caractères");
        //remet à zero la valeur stocké
        updateContactAddress(null);
        //autrement si la valeur ne correspond pas au regle de la regex
    } else if (!value.match(/^[a-zA-Zéèàêû0-9_,.-\s]*$/)) {
        //renvoi un message d'erreur
        errorDisplay("address", "L'adresse ne doit pas contenir de caractères spéciaux");
        //remet à zero la valeur stocké
        updateContactAddress(null);
        //autrement si la valeur est null
    } else if (value.length == 0) {
        //renvoi un message d'erreur
        errorDisplay("address", "Ce champs est obligatoire");
        //remet à zero la valeur stocké
        updateContactAddress(null);
        //si la valeur saisi est correct
    } else {
        //enlève le message d'erreur
        errorDisplay("address", "", true)
            //stock de ma valeur
        updateContactAddress(value);
    }
};

//vérifie la valeur saisi de l'input
//value [string] : stocke la valeur saisi
const cityChecker = (value) => {
    //si la valeur n'est pas comprise entre 3 et 30 caracteres
    if (value.length > 0 && (value.length < 2 || value.length > 50)) {
        //renvoi un message d'erreur
        errorDisplay("city", "La ville doit faire entre 2 et 50 caractères");
        //remet à zero la valeur stocké
        updateContactCity(null);
        //autrement si la valeur ne correspond pas au regle de la regex
    } else if (!value.match(/^[a-zA-Zéèàêû_.-\s]*$/)) {
        //renvoi un message d'erreur
        errorDisplay("city", "La ville ne doit pas contenir de caractères spéciaux");
        //remet à zero la valeur stocké
        updateContactCity(null);
        //autrement si la valeur est null
    } else if (value.length == 0) {
        //renvoi un message d'erreur
        errorDisplay("city", "Ce champs est obligatoire");
        //remet à zero la valeur stocké
        updateContactCity(null);
        //si la valeur saisi est correct
    } else {
        //enlève le message d'erreur
        errorDisplay("city", "", true)
            //stock de ma valeur
        updateContactCity(value);
    }
};

//vérifie la valeur saisi de l'input
//value [string] : stocke la valeur saisi
const emailChecker = (value) => {
    // si la valeur est null
    if (value.length == 0) {
        //renvoi un message d'erreur
        errorDisplay("email", "Ce champs est obligatoire");
        //remet à zero la valeur stocké
        updateContactEmail(null);
        //autrement si la valeur ne correspond pas au regle de la regex
    } else if (!value.match(/^[\w_.-]+@[\w-]+\.[a-z]{2,4}$/i)) {
        //renvoi un message d'erreur
        errorDisplay("email", "Le mail n'est pas valide");
        //si la valeur saisi est correct
    } else {
        //enlève le message d'erreur
        errorDisplay("email", "", true);
        //stock de ma valeur
        updateContactEmail(value);
    }
};

//ajoute des ecouteurs à la saisi a une zone de saisi du formulaire
inputs.forEach((input) => {

    input.addEventListener("input", (e) => {
        switch (e.target.id) {
            case "lastName":
                lastNameChecker(e.target.value)
                break;

            case "firstName":
                firstNameChecker(e.target.value)
                break;

            case "address":
                addressChecker(e.target.value)
                break;

            case "city":
                cityChecker(e.target.value)
                break;

            case "email":
                emailChecker(e.target.value)
                break;

            default:
                nul;
        }
    });
});