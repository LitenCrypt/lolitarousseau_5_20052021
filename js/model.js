//Pour mémoire
//objet qui donne la definition de l'objet que je récupere via l'api
const camera = {
    _id: String,
    imageUrl: String,
    name: String,
    price: Number,
    description: String,
    lenses: Array
}

///objets que je vais stocker dans mon local storage
// l'objet panier que je stock dans mon local storage (basket)
const basketItem = function(id, vname, imageurl, price, qte, lense) {
    this.id = id;
    this.name = vname;
    this.imgUrl = imageurl;
    this.price = price;
    this.qte = qte;
    this.lense = lense;
}

//l'objet formulaire
const formItem = function(lastName, firstName, address, city, email) {
    this.lastName = lastName;
    this.firstName = firstName;
    this.address = address;
    this.city = city;
    this.email = email;
    //flag indiquant si la valeur est valide
    this.validLastName = false;
    this.validFirstName = false;
    this.validAddress = false;
    this.validCity = false;
    this.validEmail = false;
}

//objet commande
const order = function(ids, contact) {
    this.products = ids;
    this.contact = contact;
}